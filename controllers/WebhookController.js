const Webhook = require('../models/Webhook');

module.exports = {

    async ping(req, res) {
        return res.json({ webhook: "dados do webhook", data: new Date()});
    },

    async index(req, res) {
        var url =  "https://mytimework.herokuapp.com/webhook";

        const webhooks = await Webhook.find({ url : url });

        return res.json(webhooks);
    },

    async store_old(req, res) {
        var descricao = "Webhook de testes";
        var url =  "https://mytimework.herokuapp.com/webhook";
        var data_inicial = new Date();
        var data_final = new Date();

        webhook = await Webhook.create({
                                            descricao,        
                                            url,
                                            data_inicial,
                                            data_final
                                        });

        return res.json({ webhook });
    },

    async store(req, res) {
        /*
            "id": "5bc9d29fdb171a6d480f7af9",
            "idIntegracao": "4",
            "emissao": "15/10/2018",
            "tipoAutorizacao": "WEBSERVICE",
            "situacao": "CONCLUIDO",
            "prestador": "08187168000160",
            "tomador": "00000000000191",
            "valorServico": "45",
            "numeroNfse": "5380",
            "serie": "TST",
            "lote": "35858",
            "codigoVerificacao": "1C4A09D3B",
            "autorizacao": "15/10/2018",
            "mensagem": "RPS Autorizada com sucesso",
            "pdf": "https://api.plugnotas.com.br/nfse/pdf/5bc9d29fdb171a6d480f7af9",
            "xml": "https://api.plugnotas.com.br/nfse/xml/5bc9d29fdb171a6d480f7af9"
        
        var controle = req.headers.controle;

        if(controle){
            return res.json({ controle });    
        }
        return res.json({ controle: " controle nao encontrado!"});
        */

        var body = req.body;
        var query = req.query;
        var headers = req.headers; 
        
        //return res.json({ host : headers.host });       
        //return res.json({ headers });       

        var descricao = "Webhook de testes";
        var url =  "https://mytimework.herokuapp.com/webhook";
        var data = new Date();        

        //response = "resposta:vazia";//{ body, query, headers };
        webhook = await Webhook.create({
                                            descricao,        
                                            url,
                                            data,
                                            body : JSON.stringify(body),
                                            query : JSON.stringify(query),
                                            headers : JSON.stringify(headers)
                                        });

        //return res.json({ body, query, headers });
        return res.json({ webhook });
    },

    async excluiAll(req, res) {
        const apagou = await Webhook.remove({});        
        return res.status(200).json({ sucesso: 'Todos Webhooks apagados!'});        
    }
};